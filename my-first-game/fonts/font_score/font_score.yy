{
    "id": "b0e861bd-e4d8-463d-9bc9-1dfbbefada21",
    "modelName": "GMFont",
    "mvc": "1.0",
    "name": "font_score",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Elephant",
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "6b4226f5-a53a-4117-a33b-3b0335a5b4b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 39,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "6798af15-207c-4460-9c08-942c47f864d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 463,
                "y": 84
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "742258e5-e27c-4151-aba4-8c9a680ad936",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 449,
                "y": 84
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "fc4b096c-1349-4a89-8366-4c644e8751d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 39,
                "offset": 1,
                "shift": 25,
                "w": 22,
                "x": 425,
                "y": 84
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "aa313f7f-9b70-490f-8a1e-461189fa09e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 39,
                "offset": 1,
                "shift": 23,
                "w": 20,
                "x": 403,
                "y": 84
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "73c3b344-288e-402e-89c1-52626c0ee4c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 39,
                "offset": 0,
                "shift": 31,
                "w": 31,
                "x": 370,
                "y": 84
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "ebeff888-93ac-499c-9ccd-8a4b9f2d6c6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 39,
                "offset": 1,
                "shift": 31,
                "w": 31,
                "x": 337,
                "y": 84
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "afa7926b-2e89-46a9-8ec2-06eedb86f2c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 39,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 329,
                "y": 84
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "771168ca-26c5-4617-afdd-d541e299e1cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 39,
                "offset": 2,
                "shift": 15,
                "w": 13,
                "x": 314,
                "y": 84
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "229515c8-0d27-40d4-8636-5d141b6084fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 39,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 299,
                "y": 84
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "19d4b322-e55d-4c74-9e3a-792ccf3ed91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 473,
                "y": 84
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "63789804-7d59-4659-9ef5-2d60765db45d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 39,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 277,
                "y": 84
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "6f18644e-c096-41cd-81e7-bbe5a58fbaed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 39,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 245,
                "y": 84
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "fed8e30b-0172-4cae-bccc-d9beeccd00e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 39,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 234,
                "y": 84
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5783377b-7ff6-4adb-b740-816f276cc95c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 39,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 225,
                "y": 84
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "692606a5-a610-47d2-8553-5042e82cac8c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 39,
                "offset": -2,
                "shift": 12,
                "w": 15,
                "x": 208,
                "y": 84
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "7fd976dd-3de5-443c-9876-52626d1aab60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 39,
                "offset": 1,
                "shift": 24,
                "w": 23,
                "x": 183,
                "y": 84
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "993281a6-73c8-433e-836c-f06f68116ea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 165,
                "y": 84
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1d097c5f-4e4a-4ff0-9ae4-f561a65a2c37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 39,
                "offset": 0,
                "shift": 23,
                "w": 23,
                "x": 140,
                "y": 84
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fcbee20c-3222-468a-8186-48861c9f6fff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 39,
                "offset": 0,
                "shift": 23,
                "w": 22,
                "x": 116,
                "y": 84
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "41a94bc9-7595-485f-94bb-51d579df978a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 39,
                "offset": -1,
                "shift": 22,
                "w": 23,
                "x": 91,
                "y": 84
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2564a794-f999-44c3-9821-fdbff164c217",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 39,
                "offset": 0,
                "shift": 22,
                "w": 21,
                "x": 254,
                "y": 84
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "eb732305-f784-4dc4-bb8b-d01b13e0746f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 39,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 2,
                "y": 125
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "460b5b4b-fe5d-4d36-8db7-5888fe11bd27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 23,
                "x": 25,
                "y": 125
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6ddf45b5-3b02-4ed3-aa06-66fc63e778bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 39,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 50,
                "y": 125
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4ecb8bd0-bc9d-4d3f-81b0-596fb2a9fffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 39,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 85,
                "y": 166
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "1976e4fe-533a-4060-a078-13c46fe610f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 75,
                "y": 166
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "32ad8a86-f92e-471f-bf35-c97289c5ba2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 39,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 65,
                "y": 166
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "01e28a2b-62f4-437a-adae-85d71ff59309",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 39,
                "offset": 4,
                "shift": 27,
                "w": 19,
                "x": 44,
                "y": 166
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a0bb355d-33da-455f-8686-a18f54f42b1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 39,
                "offset": 4,
                "shift": 27,
                "w": 19,
                "x": 23,
                "y": 166
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "f943c2bc-db28-4e05-a9eb-c4ec7b77e684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 39,
                "offset": 4,
                "shift": 27,
                "w": 19,
                "x": 2,
                "y": 166
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "df58c3b9-9dce-494c-92c2-3e6c364180d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 39,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 478,
                "y": 125
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a363c767-df24-4ec7-b31c-6f2926e13d36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 39,
                "offset": 1,
                "shift": 32,
                "w": 30,
                "x": 446,
                "y": 125
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "efabd786-2872-45c5-86fb-7fc4346b51c5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 39,
                "offset": -2,
                "shift": 24,
                "w": 27,
                "x": 417,
                "y": 125
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c04dc6bd-8653-4552-b9ec-b3ee886d3d04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 39,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 388,
                "y": 125
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "9377d42c-fd7d-4ee8-912e-b57576daeaa7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 39,
                "offset": 1,
                "shift": 25,
                "w": 24,
                "x": 362,
                "y": 125
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "f000f9b9-be8b-465c-9d4d-5fe9bc95a681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 39,
                "offset": 0,
                "shift": 30,
                "w": 29,
                "x": 331,
                "y": 125
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "07c8cf80-103f-43a4-b5fc-1efe518b044c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 39,
                "offset": 0,
                "shift": 28,
                "w": 27,
                "x": 302,
                "y": 125
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "77223b72-e823-4840-8895-ec18181d6919",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 39,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 275,
                "y": 125
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4b0f5ed6-1bd8-4527-8eeb-7a1f53f0a21c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 39,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 245,
                "y": 125
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "36326989-b9c6-4e6f-9b60-4a658ef47748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 39,
                "offset": 1,
                "shift": 32,
                "w": 31,
                "x": 212,
                "y": 125
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fa2d27bc-8d53-4f53-8943-dbca77de6786",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 193,
                "y": 125
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ec8705b8-34bd-4e4a-b342-1ef79a4e2c3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 39,
                "offset": -1,
                "shift": 21,
                "w": 22,
                "x": 169,
                "y": 125
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "90199b00-af63-465d-81e1-2fa9bf52a19f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 39,
                "offset": 1,
                "shift": 30,
                "w": 31,
                "x": 136,
                "y": 125
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "df8b8bfe-0ebd-4b3b-a3dd-54fff3114fef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 39,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 108,
                "y": 125
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "4bc6901d-8078-4711-a9d0-a11a13fba567",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 39,
                "offset": 0,
                "shift": 35,
                "w": 34,
                "x": 72,
                "y": 125
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "ca79719e-ed1b-48a0-8399-175325fe7486",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 39,
                "offset": -1,
                "shift": 29,
                "w": 30,
                "x": 59,
                "y": 84
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "96c27d27-b098-4065-bd2e-2372ce396487",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 39,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 30,
                "y": 84
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "97b2665d-69dd-4648-8910-0e15ec7425ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 39,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 2,
                "y": 84
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "87bde972-0fa5-4a22-b1d1-e75157fff0b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 39,
                "offset": 1,
                "shift": 29,
                "w": 28,
                "x": 2,
                "y": 43
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e8a30d63-cf2a-4db8-91e3-bd2fbe9190d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 39,
                "offset": 0,
                "shift": 29,
                "w": 30,
                "x": 457,
                "y": 2
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "ea46153d-9093-420d-8661-f25648168897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 39,
                "offset": 2,
                "shift": 23,
                "w": 20,
                "x": 435,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "600c55e1-ef8e-4224-a050-06d95f09f726",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 39,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 407,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "9e1917f7-0338-48d9-a071-3b0671c8d490",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 39,
                "offset": 0,
                "shift": 27,
                "w": 28,
                "x": 377,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "afc081f3-aa69-4b87-9cdd-1ca2b7e4da40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 39,
                "offset": -1,
                "shift": 24,
                "w": 27,
                "x": 348,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "616d7e6e-18bc-4127-bf08-5bf2314781f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 39,
                "offset": -1,
                "shift": 38,
                "w": 41,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "62343f02-d941-4464-a292-a6556975b68b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 39,
                "offset": -2,
                "shift": 24,
                "w": 29,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "4f59a4dd-071d-4535-967e-e282ab028304",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 39,
                "offset": -2,
                "shift": 24,
                "w": 28,
                "x": 244,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "af153782-99b0-4001-b911-c8c5e74ead4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 39,
                "offset": 0,
                "shift": 27,
                "w": 26,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "1a61ba34-109e-42f9-be40-88ea977fe683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 489,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "a3d45c9f-f6c1-4e87-8f1a-43869ef3056a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 39,
                "offset": -1,
                "shift": 12,
                "w": 14,
                "x": 200,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2f3a461a-e0ff-4b0c-bd87-df473a8b2eb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 39,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "fafc9149-f80c-4f61-b572-f50934b378b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 39,
                "offset": 4,
                "shift": 27,
                "w": 18,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "075e50a1-9d3f-4f46-9391-4fee1548fbba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 39,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 128,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "aa7f0f8e-e057-4d79-8b4a-3fd7412b2aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 39,
                "offset": 2,
                "shift": 16,
                "w": 9,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7ba47631-3ba4-42f9-b589-4c577a50f5f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 96,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "ef0f6eb4-eb0b-44d2-b432-d9a98da4d467",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 39,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 73,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "2a40b8dc-ecf9-4efb-bfd0-c6c329b39241",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "cf3cc69d-dfff-40a3-ac7b-1c822f1a9426",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 21,
                "x": 31,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1f02f165-e36e-4ef1-974a-2cddb634bb87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "119f64d7-f847-47c3-8ea2-164c692e52b1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 17,
                "x": 181,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "22e5eeae-6136-41c9-a90f-fdfa49270f1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 18,
                "x": 32,
                "y": 43
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b39b215f-893a-49f2-a9fc-555813285f36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 39,
                "offset": -1,
                "shift": 21,
                "w": 22,
                "x": 235,
                "y": 43
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3382dddb-b3fd-497e-af9e-001fdb971ee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 39,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 52,
                "y": 43
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "1187c883-b5e5-46c5-891d-3779a91fbb94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 39,
                "offset": -5,
                "shift": 11,
                "w": 14,
                "x": 455,
                "y": 43
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "4a911a03-9864-4f60-9d2d-6e481cae6eda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 39,
                "offset": -1,
                "shift": 20,
                "w": 23,
                "x": 430,
                "y": 43
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "fa2626e0-fef7-4b5d-b1c7-2a073dee2b9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 39,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 416,
                "y": 43
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "8cd80cdb-97a0-4268-a01d-c0b5b5b43867",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 39,
                "offset": 0,
                "shift": 30,
                "w": 31,
                "x": 383,
                "y": 43
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "913c9019-da23-4fca-9666-4ff1e8a47c4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 39,
                "offset": 0,
                "shift": 21,
                "w": 21,
                "x": 360,
                "y": 43
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "13197453-75ec-46eb-a850-7931718c0108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 39,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 340,
                "y": 43
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6edd3d4b-682b-443c-8aba-34d8164fc43f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 39,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 317,
                "y": 43
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "4bd47111-6ae2-45ff-aac3-531c566ada62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 39,
                "offset": 0,
                "shift": 20,
                "w": 20,
                "x": 295,
                "y": 43
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8793ef00-7072-407f-b999-b1294e5e14b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 39,
                "offset": 0,
                "shift": 16,
                "w": 17,
                "x": 276,
                "y": 43
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "73a53801-716c-4322-801c-455fe7fbe1a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 39,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 471,
                "y": 43
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "f9e480bd-c962-42c8-b03b-0cda8384f4e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 39,
                "offset": -1,
                "shift": 13,
                "w": 15,
                "x": 259,
                "y": 43
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5e32bdf5-bdfe-497a-bbc9-5e1b42b4975d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 39,
                "offset": -1,
                "shift": 21,
                "w": 22,
                "x": 211,
                "y": 43
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "df7a0ff1-158a-4829-8dee-37db446e090b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 39,
                "offset": -1,
                "shift": 16,
                "w": 19,
                "x": 190,
                "y": 43
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "f9d78364-84bd-4c7c-a157-cc70efd9af11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 39,
                "offset": -1,
                "shift": 26,
                "w": 28,
                "x": 160,
                "y": 43
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6319d501-1aa8-4613-9d50-511e23470cc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 39,
                "offset": -1,
                "shift": 17,
                "w": 20,
                "x": 138,
                "y": 43
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "6dfe0775-bcf6-4ee9-bbaa-a0399b25b416",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 39,
                "offset": -1,
                "shift": 16,
                "w": 19,
                "x": 117,
                "y": 43
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "88a6739c-8c10-4523-874b-a1fab9520457",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 39,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 98,
                "y": 43
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "167d2588-feef-439e-9bd6-b55cb83c7607",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 39,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 85,
                "y": 43
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "fa83a3c5-de96-46f8-8b78-cf789150333c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 39,
                "offset": 5,
                "shift": 14,
                "w": 4,
                "x": 79,
                "y": 43
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "c2f0b7c1-5bd2-4e87-9587-0a039ac81742",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 39,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 65,
                "y": 43
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "9b70531a-504b-4b14-a167-fe186774d42b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 39,
                "offset": 3,
                "shift": 27,
                "w": 20,
                "x": 108,
                "y": 166
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "b7f17d24-e2ee-4409-bf3f-4a35154ca90f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 39,
                "offset": 6,
                "shift": 31,
                "w": 19,
                "x": 130,
                "y": 166
            }
        }
    ],
    "image": null,
    "includeTTF": false,
    "italic": false,
    "kerningPairs": [
        {
            "id": "4a1743fd-adbe-49f5-aa77-8fe3e928aa6c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 34,
            "second": 44
        },
        {
            "id": "940cbb43-282b-44c0-94df-3c9823ae14b6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 34,
            "second": 46
        },
        {
            "id": "7c4ed954-436c-4399-b1ea-1778aee8a5ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 34,
            "second": 65
        },
        {
            "id": "9dd305c2-2c7f-42e9-9389-444eb9f8a037",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 34,
            "second": 198
        },
        {
            "id": "4762207d-bae1-41b5-9b43-54f38d0b3c2c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 8217
        },
        {
            "id": "4e3c861c-60e9-4efe-a592-bbacfa8444da",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 38,
            "second": 8221
        },
        {
            "id": "0e703a85-dfa4-4d53-b87e-f3cbf459621e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 44
        },
        {
            "id": "8e8a0077-951f-45e8-919a-cea5f75133fe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 39,
            "second": 46
        },
        {
            "id": "afeb3462-8aa3-4808-ba9f-e658a312b935",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 39,
            "second": 65
        },
        {
            "id": "03dd615a-995a-4bd6-a608-143d337d5a88",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 39,
            "second": 115
        },
        {
            "id": "9467b5ad-3501-4eae-9e70-a39647d59ecf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 39,
            "second": 198
        },
        {
            "id": "ab2394f0-0f04-44da-8912-0a5aeeae57e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 34
        },
        {
            "id": "0a5ca6bf-8844-40bd-9a94-ccf9f562a7cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 39
        },
        {
            "id": "e0cc5553-2367-4a78-9110-5a546def606a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 44,
            "second": 55
        },
        {
            "id": "7fcc3cfa-68f1-4aa5-85a5-f45e6249afb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 44,
            "second": 8217
        },
        {
            "id": "30a865bf-b8bc-4f46-a6bd-4f30389b429b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 44,
            "second": 8221
        },
        {
            "id": "f803b060-4a5e-4b09-ba23-6630a99e2e90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 45
        },
        {
            "id": "1d906e23-fb8c-4849-ae2c-6db16a5ebd22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 50
        },
        {
            "id": "d77d9762-fe73-474c-8f50-71f0b42bdf35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 45,
            "second": 52
        },
        {
            "id": "0bcaf98e-8296-4e3a-8f3f-306619cf23cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 45,
            "second": 55
        },
        {
            "id": "2f97482d-a515-4cb3-9f02-d35757446364",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 45,
            "second": 74
        },
        {
            "id": "eeee9152-837d-4c88-b488-024747421dbb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 84
        },
        {
            "id": "259a7f5b-f8b2-44c6-ba84-6c715b05d9bd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 86
        },
        {
            "id": "590664eb-3ff7-4a66-a60a-49582b6a6ae4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 87
        },
        {
            "id": "ee2efb86-4ae8-4c43-9830-9499c404c2f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 88
        },
        {
            "id": "a0e45688-a2c3-4de5-a2f2-e8564fba9a71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 89
        },
        {
            "id": "fff7e7d3-9618-46b8-bddf-fe671fb77cef",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 118
        },
        {
            "id": "01a04e8d-b44f-4a14-92cb-771b5ad0717a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 119
        },
        {
            "id": "9c2f7cce-0c7d-431b-a31c-a1f75a641853",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 120
        },
        {
            "id": "ead97b32-617c-4dbe-b7b5-0ccbb0f592ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 121
        },
        {
            "id": "a18cfc09-36ff-45f1-ac95-e5583f88941e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 45,
            "second": 173
        },
        {
            "id": "7f904daa-66f6-403f-863d-7b22d794b665",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 45,
            "second": 198
        },
        {
            "id": "3d020307-3905-45ce-b19e-98c3dd5d0817",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 34
        },
        {
            "id": "931a0c88-9547-4ffd-8782-4f28fa2db39e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 39
        },
        {
            "id": "b3325759-ad1a-4791-9441-4955ee8c3117",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 46,
            "second": 55
        },
        {
            "id": "dfa4edd3-a53d-4f41-8551-5fd1270d431b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 171
        },
        {
            "id": "a36439b4-55ca-4ba0-bdc5-c56418d48e5d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 46,
            "second": 8217
        },
        {
            "id": "53d087fd-f32a-4f48-a05b-0b0f2a3aac52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 46,
            "second": 8221
        },
        {
            "id": "67dadf0d-be60-4aa0-88b3-95e593cc6642",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 46,
            "second": 8249
        },
        {
            "id": "7f614d0d-db85-4714-95db-eb812a05c160",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 47,
            "second": 52
        },
        {
            "id": "b948ff1e-f907-4a18-8d86-b6356562aea9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 55,
            "second": 41
        },
        {
            "id": "3eeeac31-01f3-467d-9c32-893153c7c455",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 44
        },
        {
            "id": "f96365c5-41ef-4fc7-aa38-4b18ea88fe2a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 45
        },
        {
            "id": "2b0a379e-69de-48ef-96d0-52bfb08b0f71",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 46
        },
        {
            "id": "ee6f5d97-82fa-44c1-a5af-f89eed66df94",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 47
        },
        {
            "id": "1a8cf5ea-c8b9-4980-acf5-380c202b2dfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 55,
            "second": 52
        },
        {
            "id": "6b7d3439-503e-426c-ba28-e14970d5f3a0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 55,
            "second": 58
        },
        {
            "id": "c99e35b2-66a2-4301-bc99-998e04a8dd1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 55,
            "second": 93
        },
        {
            "id": "f4a456ae-fd24-40e2-9e73-91abee9d9f4f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 2,
            "first": 55,
            "second": 125
        },
        {
            "id": "a1ee0d71-2a5e-46ce-8a7c-551a4ef08c75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 55,
            "second": 173
        },
        {
            "id": "9914b7f9-564c-4c5f-bb85-f909b0a1428a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 55,
            "second": 176
        },
        {
            "id": "4f5b977e-05e3-40aa-bb52-053cf784463b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 58,
            "second": 55
        },
        {
            "id": "74329e10-a020-4c66-8e60-9e31634e2816",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 34
        },
        {
            "id": "1f41af43-42e5-461b-9659-7f2632fa50e3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 39
        },
        {
            "id": "34bb83cf-0e49-4bb8-a74f-0561a63c773d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 45
        },
        {
            "id": "da0eb1d1-632a-4fbe-9c7b-19a8bc397a9a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "755c8398-dfed-474d-9cf5-6bdfb10c6c17",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 85
        },
        {
            "id": "6063fd61-3e4c-4fef-9791-c96f588e5f8d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "0aec2e17-7cb7-4209-b00d-c3e6914e8425",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 87
        },
        {
            "id": "a756e3b6-131f-4ed8-933f-3f8b74885890",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 88
        },
        {
            "id": "2b97086f-5b89-473a-b61a-cc264de0322e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 65,
            "second": 89
        },
        {
            "id": "a8ee062c-cda5-4cf6-8f82-793b5088673a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 116
        },
        {
            "id": "8589b0d1-5348-441b-8b7a-cefc8ab33f52",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 117
        },
        {
            "id": "6702e5ea-e22e-4ee7-a9c4-0844fe27eeb8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 118
        },
        {
            "id": "8a90c262-a12d-42e0-96c3-41a19da27ee5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 119
        },
        {
            "id": "9c5d8b05-446c-42b4-88c8-4d80a818dc8f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 121
        },
        {
            "id": "4a15cc7f-e35b-4344-afe9-4795388c71bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 171
        },
        {
            "id": "deac5e2e-fb23-46e9-ad31-e36c8f1e5482",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 173
        },
        {
            "id": "be80ded1-e332-4e21-9dd3-e248eccdead8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 187
        },
        {
            "id": "d22d5747-26f6-4355-b0b7-2b17e05c9460",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8217
        },
        {
            "id": "ae70e57b-a500-4b58-95a4-7f705691190b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 65,
            "second": 8221
        },
        {
            "id": "47d9ec84-2185-4b8b-b3d0-fb76f9541876",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8249
        },
        {
            "id": "d9081fef-04bf-45c6-b049-7af16f00605c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8250
        },
        {
            "id": "3a524fa6-dc8e-4780-bca1-42d008d7d4af",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 45
        },
        {
            "id": "1dff3c7a-6779-42a9-b729-e361b62bba48",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 66,
            "second": 173
        },
        {
            "id": "de915edb-be02-4be7-a8a7-0ff895ac6cc4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 67,
            "second": 84
        },
        {
            "id": "0aec9763-3f6f-4dad-a698-71831a0138b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 45
        },
        {
            "id": "31307c22-fc6d-42fc-8de5-9b175aef5d66",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 69,
            "second": 84
        },
        {
            "id": "e42be452-1878-40b3-860a-eca06b70cff1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 69,
            "second": 173
        },
        {
            "id": "c092c246-1552-491b-b996-708832035577",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 44
        },
        {
            "id": "b0c22d44-2761-4d54-99ef-a93ae242ef50",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 45
        },
        {
            "id": "75c3713d-10c1-484b-aec6-8fd0363536d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 46
        },
        {
            "id": "b4e20ae8-8035-41f8-946c-eb3fd8db8733",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "c75688e3-5ea1-4220-bac4-f49817db9846",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 74
        },
        {
            "id": "e06b8928-8b44-4a02-be6c-018b908cd98e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 97
        },
        {
            "id": "027cf3f3-df68-4334-aacf-c7399e4f36aa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 101
        },
        {
            "id": "5dbbc20e-13e6-479b-9d52-a50f703faa08",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 111
        },
        {
            "id": "6313993d-0dd6-467a-9af6-f77c00f16e34",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 114
        },
        {
            "id": "8c3100f0-f833-472a-be69-13273d6026f9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 117
        },
        {
            "id": "73d0db08-1e1b-414b-b903-f04edf0bff0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 121
        },
        {
            "id": "c46bfbaa-7e6f-4e76-8876-1846c69173c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 173
        },
        {
            "id": "46c2e9ca-3b6d-4f0b-89f6-b53856559823",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 198
        },
        {
            "id": "0d9c443b-cb6e-449e-947b-c68be195264f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 230
        },
        {
            "id": "280b2fe5-0c61-419a-87aa-30db8e9a34bc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 248
        },
        {
            "id": "b871a819-a5cf-4972-8505-96d89bd5e1e2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 70,
            "second": 305
        },
        {
            "id": "dd088e4e-d0a2-4111-9dce-e2bcb8a31aff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 70,
            "second": 339
        },
        {
            "id": "d984d8d3-eb8f-4d66-b593-7f479f6c7bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8218
        },
        {
            "id": "26d38001-ab23-4c61-a003-8267837383b8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 70,
            "second": 8222
        },
        {
            "id": "b089a7f2-ae3a-4685-9e48-302fbb7e4e56",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 84
        },
        {
            "id": "4d16b3db-40b0-4ada-9abb-52db6f55cbc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 86
        },
        {
            "id": "0c6faa15-0c31-4a43-9490-8053e3072648",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 87
        },
        {
            "id": "5511a082-4caf-4078-905a-9e89f2ae5f75",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 71,
            "second": 89
        },
        {
            "id": "cadbcc2c-4294-4edb-9c8d-aa9ed58d4887",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 44
        },
        {
            "id": "786d2a28-ebc1-4ae8-9d31-5a089370977e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 46
        },
        {
            "id": "fd9cd6b0-01ee-4424-a63e-1543230dcecc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 65
        },
        {
            "id": "6fe02b4b-a060-43b7-acd4-273fb4fd3a29",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 97
        },
        {
            "id": "c62ccff5-fb5e-4c94-a5cf-a3ce3d4bfbe4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 74,
            "second": 230
        },
        {
            "id": "e34d9ccd-cbe4-40a3-a3ae-fe8920387518",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 34
        },
        {
            "id": "374aae6b-7819-49c8-bef1-c985a24c9dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 39
        },
        {
            "id": "fc313f02-086f-4910-aca8-4b82dfda5ba0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 44
        },
        {
            "id": "272e8c7a-a6a6-4bee-a1e2-929bae278bf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 75,
            "second": 46
        },
        {
            "id": "7bd190fa-62b2-4bc8-8fbf-3849484bf273",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 67
        },
        {
            "id": "a15930be-5f38-41a9-9af6-da0d5e7dc124",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 71
        },
        {
            "id": "d161a6b0-3c01-411e-915a-3f70b328f560",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 79
        },
        {
            "id": "788b04cd-038a-4f83-8844-42a4926020cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 81
        },
        {
            "id": "115a53a8-0fa8-4686-bfb7-2974b3d2042c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 84
        },
        {
            "id": "68127b67-001c-4ed9-9346-0f6ff19d812b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 85
        },
        {
            "id": "1466fb66-111f-4bfe-9e3f-9256769797a9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 86
        },
        {
            "id": "d3cfe972-8184-48e8-9805-f04b496c65a1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 87
        },
        {
            "id": "bfa848e9-4483-4935-bb74-e7d894f1fa93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 89
        },
        {
            "id": "4982045d-5eb6-4a3e-913f-8b0fcc502014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 106
        },
        {
            "id": "d268bd9e-d84d-451d-adde-b5b909ea6a7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 118
        },
        {
            "id": "b178b7d4-e1a5-449b-9c34-8133eba4b56c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 119
        },
        {
            "id": "20df0b37-b6b2-4eee-b512-f612fde875a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 121
        },
        {
            "id": "83ec5a9e-03bf-4e1e-937e-4f56fafc2618",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 216
        },
        {
            "id": "fd01e3dd-a6ff-4728-8aff-f63278768402",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 75,
            "second": 338
        },
        {
            "id": "da264412-0e98-41f1-b9dc-c8d6d9a67df5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 8217
        },
        {
            "id": "ee900c22-2f4e-4f22-baa3-816809434c23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 75,
            "second": 8221
        },
        {
            "id": "f6d3dcd8-d5af-4064-bd97-6397b94ffda8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 34
        },
        {
            "id": "176d20ef-48bc-4fe2-b3fd-53be75593d38",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 39
        },
        {
            "id": "53f9e535-6728-416c-9cb8-2dc4959af09b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 45
        },
        {
            "id": "1a75f4d1-fd8e-4a78-901d-c68ce847618b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 65
        },
        {
            "id": "135157ac-e2c9-4ece-922a-f9f195e2f322",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 67
        },
        {
            "id": "b333f4b4-93d4-4aa9-bafd-e80fd629319b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 71
        },
        {
            "id": "99cbd852-2be5-4af0-bd12-ad67039da644",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 79
        },
        {
            "id": "0f60fb09-b732-430d-a3e6-79a2e00596cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 81
        },
        {
            "id": "d07c793d-7c44-455a-a4d6-d81e3fa7d055",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 83
        },
        {
            "id": "9cf01beb-8f49-4574-8b21-aebf5ab37416",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 84
        },
        {
            "id": "82af2adf-99d2-4621-8ce3-90547902425a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 85
        },
        {
            "id": "7512b4ee-949c-4f4d-8b19-ce64994ac829",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 86
        },
        {
            "id": "00b5657c-5150-4368-8b72-6f77e7107e93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 76,
            "second": 87
        },
        {
            "id": "af8b8b95-566a-4aea-81b3-7017f2088650",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 89
        },
        {
            "id": "0e8bdd62-7d4e-4f85-934c-88fe46285fa5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 121
        },
        {
            "id": "16c7dac7-4895-4352-b936-8dfb69e1ebd2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 76,
            "second": 173
        },
        {
            "id": "3d69f1e9-2b25-473c-86cb-1b5c55458762",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 216
        },
        {
            "id": "4f0f280c-9504-4bd4-9824-b0ce975aafa9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 338
        },
        {
            "id": "5082ce7b-d804-454c-b852-672045257968",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8217
        },
        {
            "id": "a8d3e5c8-a494-470e-a764-e5db53924442",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 76,
            "second": 8221
        },
        {
            "id": "0bfc6728-0fb2-4b09-89a3-a5930d924d35",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 44
        },
        {
            "id": "004f1a33-d423-43c1-b602-88b48d65f5dc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 78,
            "second": 46
        },
        {
            "id": "f2c841ab-d15b-4ae4-82c3-513d5c3fd706",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 44
        },
        {
            "id": "4399d5dd-e6e8-47f6-8561-2619a1383d09",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 45
        },
        {
            "id": "c06b6ee8-fd69-4ab6-80d7-07ae67c4acfa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -4,
            "first": 80,
            "second": 46
        },
        {
            "id": "ee0bc9f4-d838-4172-9db0-2ba35fc6d3b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "f0e143b0-6456-430c-8632-fb2ffe69c58e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 74
        },
        {
            "id": "d990405c-3df8-4502-b6f3-773903db89c6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 97
        },
        {
            "id": "dd056bae-ac66-4e49-80f2-45aba72cb923",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 101
        },
        {
            "id": "147a8c7c-40d0-4c3f-b057-606d996ea72c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 110
        },
        {
            "id": "1fdb1b3f-e402-4c3e-8319-dde02812c328",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 111
        },
        {
            "id": "2ddc4385-6bdb-4328-81d4-6bf8291a6826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 114
        },
        {
            "id": "1d2618c2-1063-42b2-88fd-d9b640c9f8d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 115
        },
        {
            "id": "96c7f6cc-0cb2-41fe-a09c-4fd937d8dbc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 117
        },
        {
            "id": "00fb74d2-c41d-4f2f-b9cb-f2d8d6aca43c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 173
        },
        {
            "id": "d64d7c06-6843-408c-9634-8ea6b57e7ffc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 198
        },
        {
            "id": "935d22d8-aa81-4dc0-b085-3e74abc40ba7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 230
        },
        {
            "id": "ac636062-6e8c-4928-8c58-aafc1ea2efc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 248
        },
        {
            "id": "dfe4c25e-ee53-44af-8c86-26fb2c8cf16c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 80,
            "second": 339
        },
        {
            "id": "b8b7658c-9b62-4dba-bf58-7bf0d06ac076",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 8218
        },
        {
            "id": "ceb77f49-19e9-4c16-8739-a7b032d70223",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 80,
            "second": 8222
        },
        {
            "id": "325da77a-8c3b-4e14-9e0f-74704f344294",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 34
        },
        {
            "id": "37a36f76-48b0-41f1-89d2-273527d96e32",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 39
        },
        {
            "id": "891d3231-f09e-4bbc-b559-b7ab1ff511b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 45
        },
        {
            "id": "b432ac44-b258-4cc7-b33b-8b45e0a4f315",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 82,
            "second": 121
        },
        {
            "id": "2268d286-6bef-440c-9dd7-f9ae8aef5baa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 82,
            "second": 173
        },
        {
            "id": "3cb48b79-8d57-4ce5-a86e-7a3af9b87852",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 83,
            "second": 84
        },
        {
            "id": "3b9b367c-0633-4c27-9e69-781fccac3896",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 44
        },
        {
            "id": "631ff214-4c6b-4786-9a2d-f28d2d2cebb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "0734d2ae-ec7a-43de-9296-cbbbf77c6864",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 46
        },
        {
            "id": "d0aeec29-19f7-4dbd-8533-cf855b487df1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "461ee097-f4ca-40f6-a6b6-9e755bea6f1e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 74
        },
        {
            "id": "ceb0ee45-7020-481d-bde2-4be9467bd89f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 90
        },
        {
            "id": "14c209b5-e83e-4fec-9308-ae226ece9e7c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 97
        },
        {
            "id": "d3110bec-bf8f-4704-9763-456fd77fd8c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 99
        },
        {
            "id": "e0700460-1fde-455c-9948-de7cb1b7aed2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 101
        },
        {
            "id": "d32acc9f-6a4e-4095-ac91-79a8578f5745",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 111
        },
        {
            "id": "45bad0c3-a5ce-4c9b-a0f1-59d7918de148",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 114
        },
        {
            "id": "69da7958-8a7e-4ec6-924b-00d3e475626d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 115
        },
        {
            "id": "733d43c1-fd89-4c52-a99e-77c33fd4dc26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 117
        },
        {
            "id": "cb8f4634-887b-4d67-8e7d-69e87f576fdd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "4bdc0447-b1b7-4fd6-a03a-35b8694ed438",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "81307b40-eef4-48b2-90d3-bc50a082bf40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 122
        },
        {
            "id": "f71af129-9907-4b71-9bf3-4396bf680502",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 171
        },
        {
            "id": "e7707c7f-a2e0-459a-b5bb-7d0400896508",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "ac206ba4-3cff-47b2-90a9-1716640f89d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 198
        },
        {
            "id": "56abd75c-fc8c-4e69-886c-560ace3bdc2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 230
        },
        {
            "id": "4e71e8e7-843a-479e-be69-d05ea247fb77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 248
        },
        {
            "id": "43914156-7b71-4acf-afa3-3986b3c7f3ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -5,
            "first": 84,
            "second": 305
        },
        {
            "id": "19d3002b-3d3b-4a84-b207-0794e5f0eb1b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 84,
            "second": 339
        },
        {
            "id": "25d12ee0-6e24-4b6c-a445-d3d31324829e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8218
        },
        {
            "id": "49d9e062-317a-4e91-bf05-411bc5f59385",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 84,
            "second": 8222
        },
        {
            "id": "dab06820-ef5f-4469-8a3d-f6a8d74cfd0f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 8249
        },
        {
            "id": "7cf62597-90bd-4d74-b1ee-61e25750d1d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 44
        },
        {
            "id": "4bd3da54-df39-4e0b-9b77-4c24b818e674",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 46
        },
        {
            "id": "1a085d7b-b615-499a-857e-c3c735f825ce",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 65
        },
        {
            "id": "54f65815-03a3-466d-8def-ded859669e3c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 74
        },
        {
            "id": "0840fa4a-aef8-40e5-9ecf-0dcb85bca59f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 103
        },
        {
            "id": "ed88713a-9b3f-48a4-b765-45ef26b526c2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 115
        },
        {
            "id": "ed18d687-acd2-46fa-bfd5-3368d2b93dc0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 118
        },
        {
            "id": "48a52804-c46d-4c9e-919d-786786899267",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 85,
            "second": 198
        },
        {
            "id": "f2ff5da8-8390-4536-b5e4-6bef8f2cbef1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 44
        },
        {
            "id": "a48f37d4-d935-4f3c-a1e3-34ba6647ab60",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "f5c93537-8e47-4955-a4db-55e50e279de8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 46
        },
        {
            "id": "645f4293-5b5d-4fcd-bd2c-1f829376a798",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 58
        },
        {
            "id": "0d82d5f3-cc40-445c-bf38-1eb991d60bff",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 59
        },
        {
            "id": "e4c0a09b-55d8-454b-8b21-b283cf606c63",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "843b8d96-143b-41ac-b85f-dae3b82bf643",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 74
        },
        {
            "id": "0fcc2d5c-8fa9-41bf-b8cf-857404fe6585",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 97
        },
        {
            "id": "267651a2-ee60-4565-9e1c-d2e47f498bc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 101
        },
        {
            "id": "492eb416-7cb7-497c-8e74-fc4684a4bfa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 105
        },
        {
            "id": "7941ae19-b70f-49a8-ac06-3c723fecd42f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 111
        },
        {
            "id": "ca992f5f-d3df-49ae-88c5-6b3981aa16cc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 114
        },
        {
            "id": "e99e8617-626a-414b-972b-81f0d7c1e012",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 117
        },
        {
            "id": "9d5af6a1-c42b-40c1-bc4d-97ed53d95cb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 121
        },
        {
            "id": "f9e6603a-4c12-4e5f-9854-8bee52eaa472",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 171
        },
        {
            "id": "92b7737d-3130-4197-af76-4e8dec4ff868",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "6afe75c4-396f-48e3-be13-f46182d27e8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 187
        },
        {
            "id": "80cca6e5-d706-4722-a2be-de1eebd03fd9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 198
        },
        {
            "id": "88ec76bb-0637-499a-aa3a-fa77222afad5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 230
        },
        {
            "id": "865d5ad0-2ece-4c1f-a881-6a2a2bedc033",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 248
        },
        {
            "id": "8c21e396-df61-4daa-a52b-4a980e43cdcd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 305
        },
        {
            "id": "e484c8f7-63c4-43d0-87d6-dfb72152a6ae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 339
        },
        {
            "id": "959616c2-0353-4741-9f2a-ab174c445b73",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8218
        },
        {
            "id": "79f949cb-6fd5-468a-82b7-415e7da9c556",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 86,
            "second": 8222
        },
        {
            "id": "46c045c4-5f83-47eb-b3fe-e7f0f587a014",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8249
        },
        {
            "id": "93d54602-66ea-42ad-96a1-841510bce6fa",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 86,
            "second": 8250
        },
        {
            "id": "de52695f-6f20-4805-b55f-f1d6ba81f293",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 44
        },
        {
            "id": "63cf5b91-56c4-4a00-89b8-7a0fe694df90",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 45
        },
        {
            "id": "4227064f-8b65-4d72-83bc-1c1373a0cf97",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 46
        },
        {
            "id": "10a63905-9eea-4e0e-8e2e-a862353e6304",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 58
        },
        {
            "id": "0d50b9a6-b312-4dc2-b0f1-f1ec6157e09c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 59
        },
        {
            "id": "67e03028-247e-42b6-9e2d-4179ac8e7dfc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 65
        },
        {
            "id": "2782bff3-248f-4476-a572-743c96503d2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 74
        },
        {
            "id": "caa2b60f-5b35-44b1-834d-3a4b9370ae12",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 97
        },
        {
            "id": "8c87bd66-0b31-4e60-b068-d6dd98f998e1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 101
        },
        {
            "id": "3e198e1c-a9c2-484a-a9a7-e663a194fc36",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 105
        },
        {
            "id": "a15c8f89-20a5-4f5a-9fad-b5bab85c69d8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 111
        },
        {
            "id": "08788f12-a505-4236-87a8-196519676c8a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 114
        },
        {
            "id": "120ad3e1-f7df-4d3e-9dbe-84646b625c04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 117
        },
        {
            "id": "3bf2da8c-cae8-468c-8fe1-075afd281795",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 121
        },
        {
            "id": "99f77511-1abc-40ca-ba67-11d85f257838",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 171
        },
        {
            "id": "6b678d24-0df4-462a-a0d0-a8a24d5fcd64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 173
        },
        {
            "id": "50f50790-1cf1-429c-b900-d3a93a8e3d9b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 187
        },
        {
            "id": "fddbffb3-ec37-408b-921c-e4d1c7a13ee4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 198
        },
        {
            "id": "a7eaf60c-ad2f-428f-be5b-3910310512b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 230
        },
        {
            "id": "e8d79791-5910-43b2-8727-a93834c54595",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 248
        },
        {
            "id": "0e7828b2-6113-42b9-bf57-2dbe37277a28",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 305
        },
        {
            "id": "c3f4eb5f-a4bc-4e14-b589-fd6e9e253cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 339
        },
        {
            "id": "925ec20f-cb92-402c-bb1f-640278f1094c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 8218
        },
        {
            "id": "de668c91-6191-432b-bf83-c0705ac74fc9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -3,
            "first": 87,
            "second": 8222
        },
        {
            "id": "92f24cec-188c-4e4e-9006-da7d696a4372",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8249
        },
        {
            "id": "f269e7bc-6c1a-4a2d-ae4c-7dae5b0646c8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 87,
            "second": 8250
        },
        {
            "id": "4c0ba20c-bd60-4d79-bd85-5fbff42901a2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 44
        },
        {
            "id": "bd8eaad7-abaf-429b-ae8f-9d91eb893bfb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 45
        },
        {
            "id": "e0c4b9fd-0750-46f5-8c01-ef0391e7ba7b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 88,
            "second": 46
        },
        {
            "id": "fab30307-981e-4b5b-a9b7-25eafa9ee566",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 65
        },
        {
            "id": "dbcedf8a-8a13-4856-abee-940117a506a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 84
        },
        {
            "id": "8b3460cc-c9c7-44ad-8535-3d63e7547d80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 121
        },
        {
            "id": "e21eeae4-51fe-4b3d-ae27-cba14c182ff9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 171
        },
        {
            "id": "877f96f9-27f8-4163-9ac2-403525712073",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 173
        },
        {
            "id": "460ba9ba-7d12-40d2-a46f-c0e07206d46b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 88,
            "second": 187
        },
        {
            "id": "e1c1330d-e6b0-4893-94c0-7b79db2c778c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 44
        },
        {
            "id": "fa97255d-ce1d-49a1-9c34-616c71413d14",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "304176ac-4b88-47b0-afe2-42553274d8c4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 46
        },
        {
            "id": "d2a67e46-af2e-4121-bc5d-554704fb15f3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "7d9b0cd5-f9df-44b4-b11b-887ebf83e3ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "ba916d92-22ea-47c6-8c87-4bfe387b4fc5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "dbe6b64a-67d4-460d-bb35-2041e45f6b6a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 74
        },
        {
            "id": "164dd2e6-8177-4580-9bc3-bd01cdcdaa26",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 97
        },
        {
            "id": "1bdf8dae-e318-4c92-bf4b-d0dc66bc6144",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 101
        },
        {
            "id": "8641bf0f-70ec-4289-95ed-dd3d02686d64",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 103
        },
        {
            "id": "669ac630-2a37-452f-a381-b9d77fbcaaba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 111
        },
        {
            "id": "3ba3e80e-6bda-4d10-8667-682945823f05",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 112
        },
        {
            "id": "3e4b6d3f-9888-45c0-a9c1-7fdfd10609c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 113
        },
        {
            "id": "5c1dc4e3-98c7-4dbf-9249-9eed9e991c81",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 114
        },
        {
            "id": "c4da1d53-4920-4a46-ac30-de41e582d70f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "e6837db3-d9f4-404f-9f48-5843d3e63748",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 118
        },
        {
            "id": "e0c759a3-cf11-459e-a3bc-016c85f9c9ad",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 171
        },
        {
            "id": "44c4832c-1967-4395-94a6-ca2c8eb72f79",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "18297035-4096-4500-85f1-2901c9899ccd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 187
        },
        {
            "id": "b8f2f207-c357-4f35-9bf0-a3319a781a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 198
        },
        {
            "id": "f5326db9-dc8e-434b-ba04-96a44a3e73a5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 230
        },
        {
            "id": "47105506-7aa7-4676-9d04-29d65d0a23d7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 248
        },
        {
            "id": "9d5f3400-cc94-4d96-9005-ff0a0c96c7b0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 305
        },
        {
            "id": "bbb25841-7c6a-4bce-b3f3-3c0aa8542ef8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 339
        },
        {
            "id": "bcc71e3b-8d8f-4795-89f6-e37c471b6ec6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8218
        },
        {
            "id": "b4cd17b9-eba4-4e5b-9553-5c6477af0bc7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8222
        },
        {
            "id": "662d8fba-97a5-467b-9de1-6e0ca4f7149e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8249
        },
        {
            "id": "f049fa53-55f4-4960-8661-7f9367cc7b3d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 89,
            "second": 8250
        },
        {
            "id": "945567d4-3c1d-46c8-846e-8d1c9a9857d9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 118
        },
        {
            "id": "26635ad3-f1a7-47f7-9ecc-e4b65e288308",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 119
        },
        {
            "id": "40bf129d-088a-4967-b956-3bf6b82e64cb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 97,
            "second": 8217
        },
        {
            "id": "43b10aad-bb30-482c-825e-e444923eabdf",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 98,
            "second": 8217
        },
        {
            "id": "de0008aa-9408-4cf6-8bbf-d07b93c51862",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 45
        },
        {
            "id": "0ac317df-bcbd-496e-88ea-a75bdfc2398d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 99,
            "second": 84
        },
        {
            "id": "f2a997a6-7cb0-4d7b-b083-d65f3133dd40",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 86
        },
        {
            "id": "e53dcb45-2a9a-40de-818a-4718d3f1852f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 99,
            "second": 87
        },
        {
            "id": "09c7b609-1f2a-43d6-9890-f738361d0344",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 99,
            "second": 173
        },
        {
            "id": "a4c58197-aaf8-46bd-9d38-633e8a5bfcb7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 102,
            "second": 34
        },
        {
            "id": "b1f39c21-05d9-4637-8eeb-2cdd54dbb276",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 102,
            "second": 39
        },
        {
            "id": "a7d8bf22-141c-402a-8922-0e1ecf400929",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 41
        },
        {
            "id": "a16aea51-3741-49ac-9ec1-7562e386fef7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 102,
            "second": 93
        },
        {
            "id": "426f7e88-5842-41bd-a0a9-eeb4a1301049",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 102
        },
        {
            "id": "81b89bb5-e5d4-4139-ba66-59f28dd731a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 118
        },
        {
            "id": "d1b67a35-58d9-4f59-9892-46576a32c08f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 119
        },
        {
            "id": "28660a66-7a70-4ce6-81f2-2139b91e65ee",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 121
        },
        {
            "id": "c4eec28e-f4a8-42c6-b3f4-a830ca473275",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 4,
            "first": 102,
            "second": 125
        },
        {
            "id": "9f0df8a4-6925-4998-af40-d73ccb26827d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 305
        },
        {
            "id": "0fe73141-2807-49dd-a8e3-0ab7bff88659",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 102,
            "second": 8217
        },
        {
            "id": "61432371-fd47-4dff-b7c1-05f33024f099",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 5,
            "first": 102,
            "second": 8221
        },
        {
            "id": "4cecb796-7d4d-4c1c-a281-8d8aed502fa2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 61441
        },
        {
            "id": "6a113b54-76c3-42c3-a7c7-192f7fc4f51a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 61442
        },
        {
            "id": "fcdd6e4f-9943-41cf-a7d0-7f1444e29d23",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 64257
        },
        {
            "id": "7d8cde2e-e300-46fb-b2cb-f3990d080039",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 102,
            "second": 64258
        },
        {
            "id": "bb75cce2-f9b7-4f3e-ad8f-0b9c85580726",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 3,
            "first": 103,
            "second": 106
        },
        {
            "id": "68378ce1-812c-49fd-84ec-a5eb8b4da183",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": 1,
            "first": 103,
            "second": 121
        },
        {
            "id": "8c6938e9-38d8-4484-a925-6dfd99320be7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 104,
            "second": 8217
        },
        {
            "id": "7062e6eb-e995-4b1b-9adf-da65d98d5dcc",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 45
        },
        {
            "id": "fd0f730a-6bd4-4c9a-b897-299fc45f5b22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 118
        },
        {
            "id": "c6c18256-1973-4606-9cab-09d03291b319",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 119
        },
        {
            "id": "e1132237-a07a-4937-9768-b9cea62bde45",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 121
        },
        {
            "id": "d5a3966d-110f-457d-8405-051596c83b33",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 173
        },
        {
            "id": "5c20b830-3f7a-497d-bc0b-71cdf5cae1db",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 107,
            "second": 8217
        },
        {
            "id": "fd395db5-7e10-4fe8-a936-8b1787767301",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 109,
            "second": 8217
        },
        {
            "id": "f7a6a6ed-efca-40db-b910-f5ecebe90fc2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 110,
            "second": 8217
        },
        {
            "id": "841c7f01-a1fc-4ce4-a2d0-5e76bf41b454",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 111,
            "second": 8217
        },
        {
            "id": "81497dff-6cc4-4715-a1c1-201e5373bc93",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 112,
            "second": 8217
        },
        {
            "id": "4e3a4792-bafe-4cae-9b6f-b8a273c4ee55",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 44
        },
        {
            "id": "733044f4-4a3e-4927-90f6-428182eab860",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 46
        },
        {
            "id": "a301dc84-7ee0-4782-962e-a097a10934fb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 103
        },
        {
            "id": "2fcd0865-42df-4d03-a241-7ca950061a0e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8218
        },
        {
            "id": "7a017e97-e17c-4885-82f4-6adce92dcb41",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 114,
            "second": 8222
        },
        {
            "id": "8fef2df8-ed3e-4417-af38-55ba7e3b4c9f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 118
        },
        {
            "id": "a5660322-670f-4d63-bed1-09c52acf9510",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 119
        },
        {
            "id": "31d0e40c-8c0b-4b50-811c-79fe9a41684f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 116,
            "second": 121
        },
        {
            "id": "99c687f6-e57e-41a7-b433-f298b498ccbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 116,
            "second": 8217
        },
        {
            "id": "f3684d4d-c5d4-4b2c-a2b9-9395d9702fa4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 44
        },
        {
            "id": "8f1ada97-433a-4c40-8794-a7dde7961cc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 45
        },
        {
            "id": "736733ed-5b05-4355-9d7c-672ea8736a80",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 118,
            "second": 46
        },
        {
            "id": "8940c35b-cb78-41ff-889b-1753772a5b65",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 173
        },
        {
            "id": "a6ccbf7e-0b12-4af3-a2cd-2afcd24d9d1f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8218
        },
        {
            "id": "4d7e542d-8430-49f5-9a6d-93db68b0f1ab",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 8222
        },
        {
            "id": "063a02d8-a607-4771-b7b1-6502b3f2645a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 44
        },
        {
            "id": "395edf7a-f666-44d3-9802-39505119e13c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 45
        },
        {
            "id": "471b6eaf-5646-47ac-ad02-8dded5339de5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 119,
            "second": 46
        },
        {
            "id": "d725ea72-2092-4c81-9622-66209b6ce742",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 173
        },
        {
            "id": "02ca0a10-432e-4c15-a2b4-f73c9afebe11",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8218
        },
        {
            "id": "8c58a1c3-a486-4bdc-9da3-7a4914e93826",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 8222
        },
        {
            "id": "e5b580c5-690b-4297-9f63-23f864e4316e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 45
        },
        {
            "id": "b173d39a-9ff7-424c-81b3-d3df86ef13ec",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 120,
            "second": 173
        },
        {
            "id": "6b383b4b-6971-4b5e-957b-3dba72af2ae0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 44
        },
        {
            "id": "16a2d217-3598-44b2-a273-5aec7cfd821d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 45
        },
        {
            "id": "3ab2b628-bae4-4576-98a2-5389def15f2d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -2,
            "first": 121,
            "second": 46
        },
        {
            "id": "990fb526-5ccf-4999-8a27-f83e3333227b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 173
        },
        {
            "id": "59f25518-4a1e-4f0f-965a-d6c5c388b2eb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8218
        },
        {
            "id": "c325548f-bb09-419d-a17b-d12c695609f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 8222
        }
    ],
    "last": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000a\\u000aDefault Character(9647) ▯",
    "size": 24,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}