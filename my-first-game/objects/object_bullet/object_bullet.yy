{
    "id": "505fc8fe-d6c3-4f8d-b522-2867b1be37d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_bullet",
    "eventList": [
        {
            "id": "9b7c18ab-1510-4944-867b-0819195e105e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "505fc8fe-d6c3-4f8d-b522-2867b1be37d9"
        },
        {
            "id": "7c50cfd5-fa49-4e55-9870-0017ee051453",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "679289e4-88fe-4b13-87e2-55abe45e4c6f",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "505fc8fe-d6c3-4f8d-b522-2867b1be37d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ffa04d42-67aa-491d-9938-e55e4978548e",
    "visible": true
}