{
    "id": "bdb9c63f-96d7-46c7-bdc3-ef2a065406f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_title",
    "eventList": [
        {
            "id": "6e0e65f1-23ba-4e1a-86fd-c661ea7ff7c3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bdb9c63f-96d7-46c7-bdc3-ef2a065406f1"
        },
        {
            "id": "bae3b2a9-7857-47fb-9f4c-07457785f065",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bdb9c63f-96d7-46c7-bdc3-ef2a065406f1"
        },
        {
            "id": "0cf3a087-2d6b-4280-a9a8-03f91479d128",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 13,
            "eventtype": 10,
            "m_owner": "bdb9c63f-96d7-46c7-bdc3-ef2a065406f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e22f994-077a-4b82-9bcb-8d32d56c397b",
    "visible": true
}