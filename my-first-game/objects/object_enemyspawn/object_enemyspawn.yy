{
    "id": "a212846e-a850-4cba-a157-9ca25023bb11",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_enemyspawn",
    "eventList": [
        {
            "id": "623c5d9a-b0d7-4af3-aa45-27f6521d949c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a212846e-a850-4cba-a157-9ca25023bb11"
        },
        {
            "id": "9b5b0a6a-3607-4855-aa68-69d73cfba5fc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a212846e-a850-4cba-a157-9ca25023bb11"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4b18db42-a2bb-45b6-abf4-7c196ec770d2",
    "visible": true
}