{
    "id": "679289e4-88fe-4b13-87e2-55abe45e4c6f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "object_enemy",
    "eventList": [
        {
            "id": "6a19f60a-e1e3-420f-a67b-de8987ec6f52",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "679289e4-88fe-4b13-87e2-55abe45e4c6f"
        },
        {
            "id": "cc4980d3-6d77-42d0-9c02-621d93faa8ac",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "679289e4-88fe-4b13-87e2-55abe45e4c6f"
        },
        {
            "id": "1cc410b1-af66-4cd8-826c-232f72d4a334",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "a79ddaaf-ac33-42a9-aa1a-a95fe6f00239",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "679289e4-88fe-4b13-87e2-55abe45e4c6f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4b18db42-a2bb-45b6-abf4-7c196ec770d2",
    "visible": true
}