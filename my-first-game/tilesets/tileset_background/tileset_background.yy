{
    "id": "52314c1c-7645-4458-be51-2d8ce961da28",
    "modelName": "GMTileSet",
    "mvc": "1.11",
    "name": "tileset_background",
    "auto_tile_sets": [
        
    ],
    "macroPageTiles": {
        "SerialiseData": null,
        "SerialiseHeight": 0,
        "SerialiseWidth": 0,
        "TileSerialiseData": [
            
        ]
    },
    "out_columns": 3,
    "out_tilehborder": 2,
    "out_tilevborder": 2,
    "spriteId": "4d71911c-1531-4c5c-81f8-a1defa5e76cd",
    "sprite_no_export": true,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "tile_animation": {
        "AnimationCreationOrder": null,
        "FrameData": [
            0,
            1,
            2,
            3,
            4,
            5,
            6,
            7,
            8,
            9,
            10,
            11
        ],
        "SerialiseFrameCount": 1
    },
    "tile_animation_frames": [
        
    ],
    "tile_animation_speed": 15,
    "tile_count": 12,
    "tileheight": 128,
    "tilehsep": 0,
    "tilevsep": 0,
    "tilewidth": 128,
    "tilexoff": 0,
    "tileyoff": 0
}