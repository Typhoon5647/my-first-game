{
    "id": "ffa04d42-67aa-491d-9938-e55e4978548e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 70,
    "bbox_left": 97,
    "bbox_right": 109,
    "bbox_top": 55,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "17bbe220-3f4e-4d65-ba87-a957f46ab062",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ffa04d42-67aa-491d-9938-e55e4978548e",
            "compositeImage": {
                "id": "e49d5a60-3d37-4e08-8c06-c0732f35504d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17bbe220-3f4e-4d65-ba87-a957f46ab062",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00dabc1b-143e-4193-be37-f1fe1c019397",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17bbe220-3f4e-4d65-ba87-a957f46ab062",
                    "LayerId": "37391cee-c84d-4bdf-8101-29b3bc63b297"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "37391cee-c84d-4bdf-8101-29b3bc63b297",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ffa04d42-67aa-491d-9938-e55e4978548e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 106,
    "yorig": 63
}