{
    "id": "973837ef-9b50-48f8-91cb-a3421993d42f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_darktile",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 127,
    "bbox_left": 0,
    "bbox_right": 127,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "35f894d5-ee75-4e7e-b41f-d7bf51099220",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "973837ef-9b50-48f8-91cb-a3421993d42f",
            "compositeImage": {
                "id": "42192aa3-9fb7-4138-9acb-1bdd530d49dc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35f894d5-ee75-4e7e-b41f-d7bf51099220",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "31082b81-c9c0-4709-8cfa-446c6c3f3eee",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35f894d5-ee75-4e7e-b41f-d7bf51099220",
                    "LayerId": "8c773162-f83e-468e-9d36-091c58e1ebac"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 128,
    "layers": [
        {
            "id": "8c773162-f83e-468e-9d36-091c58e1ebac",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "973837ef-9b50-48f8-91cb-a3421993d42f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 128,
    "xorig": 0,
    "yorig": 0
}