{
    "id": "c4db90d6-5052-45c7-a18c-4680f7104309",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_player",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 83,
    "bbox_left": 27,
    "bbox_right": 85,
    "bbox_top": 30,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "7d795fc3-64c1-431a-a270-b599f585265f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c4db90d6-5052-45c7-a18c-4680f7104309",
            "compositeImage": {
                "id": "4f431956-9c3e-47f9-b028-aab85464434d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "7d795fc3-64c1-431a-a270-b599f585265f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "04842de6-d5a8-4fea-9aea-a11b80d563d2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "7d795fc3-64c1-431a-a270-b599f585265f",
                    "LayerId": "9bf41d48-54c6-4dc0-b6be-aaf3114b54df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 115,
    "layers": [
        {
            "id": "9bf41d48-54c6-4dc0-b6be-aaf3114b54df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c4db90d6-5052-45c7-a18c-4680f7104309",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 114,
    "xorig": 57,
    "yorig": 57
}