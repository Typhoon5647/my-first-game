{
    "id": "0e22f994-077a-4b82-9bcb-8d32d56c397b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_titlecreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 282,
    "bbox_left": 0,
    "bbox_right": 859,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f405b98b-79c9-4a65-83c7-5d70f2ae14cf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "0e22f994-077a-4b82-9bcb-8d32d56c397b",
            "compositeImage": {
                "id": "40fa97f5-5ced-46b8-a6fe-57cbacb9d74f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f405b98b-79c9-4a65-83c7-5d70f2ae14cf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "27feeddf-6d21-4a4c-ab89-6b8e0f5e3e07",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f405b98b-79c9-4a65-83c7-5d70f2ae14cf",
                    "LayerId": "cd1e5a77-55c6-44db-931c-00c77d1a616a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 283,
    "layers": [
        {
            "id": "cd1e5a77-55c6-44db-931c-00c77d1a616a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "0e22f994-077a-4b82-9bcb-8d32d56c397b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 860,
    "xorig": 430,
    "yorig": 141
}