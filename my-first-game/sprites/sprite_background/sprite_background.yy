{
    "id": "4d71911c-1531-4c5c-81f8-a1defa5e76cd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_background",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 383,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "322dd433-d12f-45da-9f0d-6217af6288e1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4d71911c-1531-4c5c-81f8-a1defa5e76cd",
            "compositeImage": {
                "id": "731526f3-49f5-4b96-8aac-d7ce98497c3f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "322dd433-d12f-45da-9f0d-6217af6288e1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "134936ae-b8e6-437e-a605-16bee5eaa113",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "322dd433-d12f-45da-9f0d-6217af6288e1",
                    "LayerId": "c7cc098f-12f5-470f-8914-c0f7b58a9da3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "c7cc098f-12f5-470f-8914-c0f7b58a9da3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4d71911c-1531-4c5c-81f8-a1defa5e76cd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 384,
    "xorig": 0,
    "yorig": 0
}