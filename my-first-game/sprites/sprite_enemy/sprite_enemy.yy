{
    "id": "4b18db42-a2bb-45b6-abf4-7c196ec770d2",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 87,
    "bbox_left": 32,
    "bbox_right": 80,
    "bbox_top": 37,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "cb38d7a1-4773-4ec9-9e28-833f2116e0b5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4b18db42-a2bb-45b6-abf4-7c196ec770d2",
            "compositeImage": {
                "id": "974c7749-bf50-4137-9c5b-a9a0916c4515",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cb38d7a1-4773-4ec9-9e28-833f2116e0b5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8a7c0e0e-4e6a-4881-802a-c9ec2b4e0105",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cb38d7a1-4773-4ec9-9e28-833f2116e0b5",
                    "LayerId": "2560e13b-50bd-4a8a-824f-80657f18594c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 114,
    "layers": [
        {
            "id": "2560e13b-50bd-4a8a-824f-80657f18594c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4b18db42-a2bb-45b6-abf4-7c196ec770d2",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": true,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 115,
    "xorig": 57,
    "yorig": 57
}